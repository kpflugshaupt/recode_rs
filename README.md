# recode_rs

recode_rs is a command-line tool converting between the character encodings
defined in the [Encoding Standard][1].

It is written primarily as sample code that demonstrates the use of
[encoding_rs][2].

[1]: https://encoding.spec.whatwg.org/

[2]: https://github.com/hsivonen/encoding_rs

## Installing via `cargo`

Using release-channel Rust:

```
cargo install recode_rs
```

With SIMD acceleration on x86, x86_64 and Aarch64:

```
cargo install recode_rs --features simd-accel
```

## Building from a local git clone

Using release-channel Rust:

```
cargo build --release
```

With SIMD acceleration on x86, x86_64 and Aarch64:

```
cargo build --release --features simd-accel
```

## Usage

```
recode_rs [-f INPUT_ENCODING] [-t OUTPUT_ENCODING] [-o OUTFILE] [INFILE] [...]
```

### Options

```
    -o, --output PATH   set output file name (- for stdout; the default)
    -f, --from-code LABEL
                        set input encoding (defaults to UTF-8)
    -t, --to-code LABEL set output encoding (defaults to UTF-8)
    -h, --help          print usage help
```

